using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScreenAnimationBase : MonoBehaviour
{
    [Header("UI Canvas Container")]
    public Transform targetUIElement; // Canvas we are going to control.

    [Header("Transition's duration")]
    public float durationIn;       // Duration of the animation OUT -> IN
    public float durationOut;      // Diration of the animation IN -> OUT


    public abstract void AnimateIn(Action OnAnimationFinish);
    public abstract void AnimateOut(Action OnAnimationFinish);

    

    protected float GetHorizontalFarPoint()
    {
        return GetComponentInChildren<Canvas>(true).GetComponent<RectTransform>().rect.width;
    }

    protected float GetVerticalFarPoint()
    {
        return GetComponentInChildren<Canvas>(true).GetComponent<RectTransform>().rect.height;
    }


}
