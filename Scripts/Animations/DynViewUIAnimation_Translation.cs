
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Device;
using UnityEngine.UIElements;

namespace DynViewUI
{
    public class DynViewUIAnimation_Translation : ScreenAnimationBase
{

        public enum Direction { UP, DOWN, LEFT, RIGHT };

        public float timeIn = 1;
        public LeanTweenType easeIn;
        public Direction directionIn = Direction.LEFT;
        public float timeOut = 1;
        public LeanTweenType easeOut;
        public Direction directionOut = Direction.RIGHT;


        private void Awake()
        {

        }
        public override void AnimateIn(Action OnAnimationFinish)
        {
            RectTransform rectTarget = (RectTransform)targetUIElement;

            Vector3 fromPosition = CalculateHiddingPosition(this.directionIn);
            Vector3 toPosition = Vector3.zero;

            // Performs the animation
            LeanTween.value(0, 1, timeIn).setEase(easeIn).setOnComplete(() =>
            {
                OnAnimationFinish?.Invoke();
            }).setOnUpdate((float value) =>
            {
                rectTarget.localPosition = Vector3.Lerp(fromPosition, toPosition, value);
            });

        }

        public override void AnimateOut(Action OnAnimationFinish)
        {
            RectTransform rectTarget = (RectTransform)targetUIElement;

            Vector3 fromPosition = rectTarget.localPosition;
            Vector3 toPosition = CalculateHiddingPosition(this.directionOut);




            LeanTween.value(0, 1, timeOut).setEase(easeOut).setOnComplete(() =>
            {
                OnAnimationFinish?.Invoke();
            }).setOnUpdate((float value) =>
            {
                rectTarget.localPosition = Vector3.Lerp(fromPosition, toPosition, value);
            });
        }


        private Vector3 CalculateHiddingPosition(Direction animationDirection)
        {
            Vector3 result = Vector3.zero;

            switch (animationDirection)
            {
                case Direction.UP:
                    result = new Vector3(0, GetVerticalFarPoint(), 0);
                    break;
                case Direction.DOWN:
                    result = new Vector3(0, -GetVerticalFarPoint(), 0);
                    break;
                case Direction.LEFT:
                    result = new Vector3(GetHorizontalFarPoint(), 0, 0);
                    break;
                case Direction.RIGHT:
                    result = new Vector3(-GetHorizontalFarPoint(), 0, 0);
                    break;
            }

            return result;
        }
    }

}
