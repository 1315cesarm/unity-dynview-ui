using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



namespace DynViewUI
{
    public class DynViewUIAnimationEvents : MonoBehaviour
    {

        public UnityEvent beforeOpenEvent;
        public UnityEvent afterOpenEvent;
        public UnityEvent beforeCloseEvent;
        public UnityEvent afterCloseEvent;

        public void ClearListeners()
        {
            beforeOpenEvent.RemoveAllListeners();
            afterOpenEvent.RemoveAllListeners();
            beforeCloseEvent.RemoveAllListeners();
            afterCloseEvent.RemoveAllListeners();
        }
        public void PrintDebug(string message)
        {
            Debug.Log(message);
        }

    }
}


