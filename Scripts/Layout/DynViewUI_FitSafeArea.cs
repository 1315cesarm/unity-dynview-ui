using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DynViewUI
{
    public class DynViewUI_FitSafeArea : MonoBehaviour
    {
        RectTransform rectTransform;
        Rect lastSafeArea;
        public Canvas canvas;
        void Awake()
        {
            this.rectTransform = (RectTransform)this.transform;
        }

        // Update is called once per frame
        void Update()
        {
            if (Screen.safeArea != this.lastSafeArea && Screen.safeArea != null)
            {
                Debug.Log("SAFE AREA!");
                this.lastSafeArea = Screen.safeArea;

                var safeArea = Screen.safeArea;

                var anchorMin = safeArea.position;
                var anchorMax = safeArea.position + safeArea.size;
                anchorMin.x /= canvas.pixelRect.width;
                anchorMin.y /= canvas.pixelRect.height;
                anchorMax.x /= canvas.pixelRect.width;
                anchorMax.y /= canvas.pixelRect.height;

                rectTransform.anchorMin = anchorMin;
                rectTransform.anchorMax = anchorMax;

            }

        }
    }
}

