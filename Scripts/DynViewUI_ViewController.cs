using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace DynViewUI
{
    public class DynViewUI_ViewController : MonoBehaviour
    {

        [Header("Canvas (manually setted)")]
        public Canvas canvas;
        public CanvasGroup canvasGroup;


        [HideInInspector] public DynViewUIAnimationEvents screenEvents;

        [HideInInspector] public ScreenAnimationBase screenAnimation;

        private void Awake()
        {

            this.canvasGroup = canvas.gameObject.GetComponent<CanvasGroup>();

            this.screenAnimation = GetComponent<ScreenAnimationBase>();

            this.screenEvents = GetComponent<DynViewUIAnimationEvents>();
        }

        public void Show(Action onFinish = null)
        {
            this.canvasGroup.interactable = false;

            screenEvents?.beforeOpenEvent.Invoke();

            canvas.gameObject.SetActive(true);

            if(screenAnimation != null)
            {
                //screenAnimation.target = canvas.transform.GetChild(0);
                screenAnimation.AnimateIn(() =>
                {
                    screenEvents?.afterOpenEvent.Invoke();
                    onFinish?.Invoke();
                    this.canvasGroup.interactable = true;
                });
            }
            else
            {
                screenEvents?.afterOpenEvent.Invoke();
                onFinish?.Invoke();
                this.canvasGroup.interactable = true;
            }

        }

        public void Hide(Action onFinish = null)
        {
            this.canvasGroup.interactable = false;
            screenEvents?.beforeCloseEvent.Invoke();

            if (screenAnimation != null)
            {
                screenAnimation.AnimateOut(() =>
                {
                    canvas.gameObject.SetActive(false);
                    screenEvents?.afterCloseEvent.Invoke();
                    onFinish?.Invoke();
                    
                });
            }
            else
            {
                canvas.gameObject.SetActive(false);
                screenEvents?.afterCloseEvent.Invoke();
                onFinish?.Invoke();

            }
        }

    }
}