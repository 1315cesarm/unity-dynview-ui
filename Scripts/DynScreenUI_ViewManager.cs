using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace DynViewUI
{
    public class DynScreenUI_ViewManager : MonoBehaviour
{
        [Serializable]
        public class ScreenItem
        {
            public string id;
            public DynViewUI_ViewController screen;
        }


        public List<ScreenItem> screenItems;

        public bool show;
        public bool hide;

        public ScreenItem activeScreen = null;


        public void ShowScreen(string id)
        {
            foreach(var element in screenItems)
            {
                if(element.id.ToLower() == id.ToLower())
                {
                    activeScreen?.screen?.Hide();
                    element.screen.Show();

                    activeScreen = element;
                    return;
                }
            }


            Debug.LogError("Screen id not found: " + id, this);
        }

        private void Update()
        {
            if (show)
            {
                show = false;

                screenItems[1].screen.Hide();
                screenItems[0].screen.Show();
            }

            if (hide)
            {
                hide = false;

                screenItems[0].screen.Hide();
                screenItems[1].screen.Show();
            }
        }


    }

}
